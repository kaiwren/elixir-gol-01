defmodule Gol.Game do
  use Agent
  alias Gol.Grid

  @spec start_link(Grid.grid) :: Agent.on_start()
  def start_link(seed_grid) do
    Agent.start_link(fn -> seed_grid end)
  end

  @spec get_state(Grid.grid_pid) :: Grid.grid
  def get_state(grid_pid) do
    Agent.get(grid_pid, fn value -> value end)
  end

  @spec update_to_next_generation(Grid.grid_pid) :: :ok
  def update_to_next_generation(grid_pid) do
    Agent.update(grid_pid, fn grid ->
      Grid.next_generation(grid)
    end)
  end

  @spec update_n_generations(Grid.grid_pid, integer()) :: [:ok]
  def update_n_generations(grid_pid, n) do
    for _ <- 0..n, do: Gol.Game.update_to_next_generation(grid_pid)
  end

  @spec update_grid_at(Grid.grid_pid, integer(), integer(), Grid.cell) :: :ok
  def update_grid_at(grid_pid, row_number, column_number, value) do
    Agent.update(grid_pid, fn grid ->
      Grid.update_cell_at(grid, row_number, column_number, value)
    end)
  end
end
