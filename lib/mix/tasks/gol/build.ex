defmodule Mix.Tasks.Gol.Build do
  @moduledoc "Build the codebase"
  @shortdoc "Build the codebase: dialyzer and test"

  use Mix.Task

  @impl Mix.Task
  def run(_) do
    Mix.Task.run("dialyzer")
    Mix.Task.run("test")
  end
end
