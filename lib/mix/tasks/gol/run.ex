defmodule Mix.Tasks.Gol.Run do
  @moduledoc "Run Game of Life"
  @shortdoc "Run Game of Life; valid arguments are: [blinker|glider|gosper] [n]"

  use Mix.Task

  @impl Mix.Task
  def run(args) do
    [arg_1, arg_2] = args
    seed = case arg_1 do
      "blinker" -> Gol.Seeds.blinker_vertical()
      "glider" -> Gol.Seeds.glider()
      "gosper" -> Gol.Seeds.gospers_glider_gun()
      _ -> raise "No idea what this means!?"
    end

    n = if not is_nil(arg_2), do: String.to_integer(arg_2), else: 10
    {:ok, pid} = Gol.Game.start_link(seed)

    for _ <- 0..n do
      Gol.Game.get_state(pid)
      |> Kernel.inspect(pretty: true)
      |> Mix.shell().info()

      Gol.Game.update_to_next_generation(pid)
    end
  end
end
