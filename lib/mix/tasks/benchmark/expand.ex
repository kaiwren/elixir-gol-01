defmodule Mix.Tasks.Benchmark.Expand do
  @moduledoc "Run benchmarks for expanding the Grid"
  @shortdoc "Run benchmarks for expanding the Grid"

  use Mix.Task

  @impl Mix.Task
  def run(_) do
    blinker_grid = Gol.Seeds.blinker_vertical()

    n = 500

    Benchee.run(%{
      "expand grid" => fn ->
        Enum.reduce(0..n, blinker_grid, fn _, acc -> Gol.Grid.expand(acc) end)
      end
    })
  end
end
