defmodule Mix.Tasks.Benchmark.Game do
  @moduledoc "Run benchmarks for Game of Life"
  @shortdoc "Run benchmarks for Game of Life"

  use Mix.Task

  @impl Mix.Task
  def run(_) do
    {:ok, blinker_pid} = Gol.Game.start_link(Gol.Seeds.blinker_vertical())
    {:ok, block_pid} = Gol.Game.start_link(Gol.Seeds.block())
    {:ok, glider_pid} = Gol.Game.start_link(Gol.Seeds.glider())

    n = 500

    Benchee.run(%{
      "blinker" => fn -> Gol.Game.update_n_generations(blinker_pid, n) end,
      "block" => fn -> Gol.Game.update_n_generations(block_pid, n) end,
      "glider" => fn -> Gol.Game.update_n_generations(glider_pid, n) end
    })
  end
end
