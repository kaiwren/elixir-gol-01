defmodule Gol.Neighbours do
  alias Gol.Grid
  @type neighbours :: %{integer() => %{integer() => 0..8}}

  @spec calculate_live_neighbour_counts(Grid.grid) :: neighbours
  def calculate_live_neighbour_counts(grid) do
    max = Kernel.length(grid) - 1

    for row_number <- 0..max, column_number <- 0..max do
      count_of_live_neighbours =
        for row_offset <- [-1, 0, 1], column_offset <- [-1, 0, 1] do
          offset_row_number = row_number + row_offset
          offset_column_number = column_number + column_offset

          if {row_offset, column_offset} != {0, 0} and
               offset_row_number <= max and offset_row_number >= 0 and
               offset_column_number <= max and offset_row_number >= 0 do
            grid
            |> Enum.at(offset_row_number)
            |> Enum.at(offset_column_number)
            |> Kernel.then(fn cell_value -> if cell_value == Grid.live_cell(), do: 1, else: 0 end)
          else
            0
          end
        end
        |> Enum.sum()

      {row_number, column_number, count_of_live_neighbours}
    end
    |> Enum.reduce(%{}, fn {row_number, column_number, count}, accumulator ->
      row = Map.get(accumulator, row_number) || %{}
      updated_row = Map.put(row, column_number, count)
      Map.put(accumulator, row_number, updated_row)
    end)
  end
end
