defmodule Gol.Grid do
  alias Gol.Matrix
  alias Gol.Neighbours
  @type grid_pid :: pid()
  @type live_cell :: 1
  @type dead_cell :: 0
  @type cell :: live_cell | dead_cell
  @type grid :: [[cell]]

  @spec build_seed_grid_state(integer) :: grid
  def build_seed_grid_state(max) do
    row_state = List.duplicate(dead_cell(), max)
    for _ <- 0..(max - 1), into: [], do: row_state
  end

  @spec next_generation(grid) :: grid
  def next_generation(grid) do
    neighbours = Neighbours.calculate_live_neighbour_counts(grid)
    max = Kernel.length(grid) - 1
    grid_map = Matrix.from_list(grid)

    grid_updates =
      for row <- 0..max, column <- 0..max do
        cell = grid_map[row][column]
        live_neighbour_count = neighbours[row][column]

        if cell == dead_cell() and live_neighbour_count == 3 do
          {row, column, live_cell()}
        else
          if cell == live_cell() and (live_neighbour_count < 2 or live_neighbour_count > 3) do
            {row, column, dead_cell()}
          end
        end
      end
      |> Enum.reject(&is_nil/1)

    updated_grid_map =
      grid_updates
      |> Enum.reduce(grid_map, fn {row_number, column_number, cell}, accumulator ->
        Kernel.put_in(accumulator, [row_number, column_number], cell)
      end)

    grid = Matrix.to_list(updated_grid_map)

    live_cell_on_borders =
      grid_updates
      |> Enum.find(fn {row, column, cell} ->
        cell == live_cell() and (row == 0 or row == max or column == 0 or column == max)
      end)

    if not is_nil(live_cell_on_borders), do: expand(grid), else: grid
  end

  @spec update_cell_at(grid, integer, integer, cell) :: grid
  def update_cell_at(grid, row_number, column_number, value) do
    {row_state, intermediate_grid} = List.pop_at(grid, row_number)
    updated_row_state = List.replace_at(row_state, column_number, value)
    List.insert_at(intermediate_grid, row_number, updated_row_state)
  end

  @spec expand(grid) :: grid
  def expand(grid) do
    max = Kernel.length(grid)

    grid_with_rows_expanded =
      grid
      |> List.insert_at(0, List.duplicate(dead_cell(), max))
      |> List.insert_at(-1, List.duplicate(dead_cell(), max))

    _grid_with_columns_and_rows_expanded =
      for row_state <- grid_with_rows_expanded, do: [dead_cell()] ++ row_state ++ [dead_cell()]
  end

  @spec dead_cell :: dead_cell
  def dead_cell do
    0
  end

  @spec live_cell :: live_cell
  def live_cell do
    1
  end
end
