defmodule Gol.GridTest do
  use ExUnit.Case, async: true
  alias Gol.Grid

  test "cell at row, column can be updated" do
    expected_grid_state = [
      [0, 0, 0],
      [0, 1, 0],
      [0, 0, 0]
    ]

    assert Grid.update_cell_at(Grid.build_seed_grid_state(3), 1, 1, 1) == expected_grid_state
  end

  test "grid can be expanded" do
    expected_grid_state = [
      [0, 0, 0, 0, 0],
      [0, 0, 1, 0, 0],
      [0, 1, 0, 1, 0],
      [0, 0, 1, 0, 0],
      [0, 0, 0, 0, 0]
    ]

    assert Grid.expand([
             [0, 1, 0],
             [1, 0, 1],
             [0, 1, 0]
           ]) == expected_grid_state
  end

  test "generates next generation of vertical blinker" do
    assert Grid.next_generation(Gol.Seeds.blinker_vertical()) ==
             Gol.Seeds.blinker_horizontal()
  end

  test "generates vertical blinker two generations after seeding with vertical blinker" do
    final_grid =
      Gol.Seeds.blinker_vertical()
      |> Grid.next_generation()
      |> Grid.next_generation()

    assert final_grid == Gol.Seeds.blinker_vertical()
  end

  test "moving glider expands the grid" do
    final_grid = Gol.Seeds.glider()
    |> Grid.next_generation()
    |> Grid.next_generation()

    assert final_grid == [
             [0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 1, 0, 0],
             [0, 0, 1, 0, 1, 0, 0],
             [0, 0, 0, 1, 1, 0, 0],
             [0, 0, 0, 0, 0, 0, 0]
           ]
  end
end
