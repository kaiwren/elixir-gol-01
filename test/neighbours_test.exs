defmodule Gol.NeighboursTest do
  use ExUnit.Case
  alias Gol.Neighbours

  test "live neighbour count is all zero when there are no live cells" do
    seed_grid = [
      [0, 0, 0],
      [0, 0, 0],
      [0, 0, 0]
    ]

    expected_neighbours = %{
      0 => %{0 => 0, 1 => 0, 2 => 0},
      1 => %{0 => 0, 1 => 0, 2 => 0},
      2 => %{0 => 0, 1 => 0, 2 => 0}
    }

    assert Neighbours.calculate_live_neighbour_counts(seed_grid) == expected_neighbours
  end

  test "live neighbour count can be computed when there's a single live cell" do
    seed_grid =[
         [0, 0, 0],
         [0, 1, 0],
         [0, 0, 0]
       ]

    expected_neighbours =
       %{
         0 => %{0 => 1, 1 => 1, 2 => 1},
         1 => %{0 => 1, 1 => 0, 2 => 1},
         2 => %{0 => 1, 1 => 1, 2 => 1}
       }

    assert Neighbours.calculate_live_neighbour_counts(seed_grid) == expected_neighbours
  end

  test "live neighbour count can be computed when there are multiple live cells" do
    seed_grid = [
         [0, 0, 0, 0, 0],
         [0, 0, 1, 0, 0],
         [0, 0, 1, 0, 0],
         [0, 0, 1, 0, 0],
         [0, 0, 0, 0, 0]
       ]

    expected_neighbours =
       %{
         0 => %{0 => 0, 1 => 1, 2 => 1, 3 => 1, 4 => 0},
         1 => %{0 => 0, 1 => 2, 2 => 1, 3 => 2, 4 => 0},
         2 => %{0 => 0, 1 => 3, 2 => 2, 3 => 3, 4 => 0},
         3 => %{0 => 0, 1 => 2, 2 => 1, 3 => 2, 4 => 0},
         4 => %{0 => 0, 1 => 1, 2 => 1, 3 => 1, 4 => 0}
       }

    assert Neighbours.calculate_live_neighbour_counts(seed_grid) == expected_neighbours
  end
end
