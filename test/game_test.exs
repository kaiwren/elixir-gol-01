defmodule Gol.GameTest do
  use ExUnit.Case, async: true
  alias Gol.Game
  alias Gol.Grid

  test "initialized state is preserved" do
    {:ok, game_pid} = Game.start_link(Grid.build_seed_grid_state(5))

    expected_grid = [
      [0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0]
    ]

    assert Game.get_state(game_pid) == expected_grid
  end

  test "cell at row, column can be updated" do
    {:ok, game_pid} = Game.start_link(Grid.build_seed_grid_state(3))

    expected_grid_state = [
      [0, 0, 0],
      [0, 1, 0],
      [0, 0, 0]
    ]

    assert Game.update_grid_at(game_pid, 1, 1, 1) == :ok
    assert Game.get_state(game_pid) == expected_grid_state
  end

  test "generates next generation of vertical blinker" do
    {:ok, game_pid} =
      Game.start_link(Gol.Seeds.blinker_vertical())

    horizontal_blinker = Gol.Seeds.blinker_horizontal()

    assert Game.update_to_next_generation(game_pid) == :ok
    assert Game.get_state(game_pid) == horizontal_blinker
  end

  test "generates vertical blinker two generations after seeding with vertical blinker" do
    vertical_blinker_seed = Gol.Seeds.blinker_vertical()

    {:ok, game_pid} = Game.start_link(vertical_blinker_seed)

    assert Game.update_to_next_generation(game_pid) == :ok
    assert Game.update_to_next_generation(game_pid) == :ok
    assert Game.get_state(game_pid) == vertical_blinker_seed
  end

  test "moving glider expands the grid" do
    glider_seed = Gol.Seeds.glider()

    {:ok, game_pid} = Game.start_link(glider_seed)

    assert Game.update_to_next_generation(game_pid) == :ok
    assert Game.update_to_next_generation(game_pid) == :ok

    assert Game.get_state(game_pid) == [
             [0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 1, 0, 0],
             [0, 0, 1, 0, 1, 0, 0],
             [0, 0, 0, 1, 1, 0, 0],
             [0, 0, 0, 0, 0, 0, 0]
           ]
  end
end
